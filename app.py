import json
from flask import Flask, jsonify, request
import db


app = Flask(__name__)


#books endpoint.
@app.route('/books', methods=['GET','POST'])
def books_endpoint():
    if request.method == 'GET':
        return json.dumps(db.get_all_books())
    else:
        return jsonify(db.insert_new_book(request.args['title'], request.args['author']))



#handling 404 error
@app.errorhandler(404)
def notFound(error=None):
    message = {
      'status': 404,
      'message': 'Not Found: ' + request.url,
    }
    response = jsonify(message)
    response.status_code = 404
    return response


if __name__ == "__main__":
    app.run(host ='0.0.0.0')