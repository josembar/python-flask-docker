import mysql.connector


#credentials to connect to database
config = {
  'user': 'root',
  'password': 'p@ssw0rd1',
  'host': 'mysqldb',
  'database': 'library'
}

#remove database to config
def remove_database_config():
    remove_database_config = config.copy()
    remove_database_config.pop('database')
    return remove_database_config



#creates connection to mysql
def create_connection(configuration):
    connection = mysql.connector.connect(**configuration)
    return connection


#run any query
def run_query(connection, query):
    cursor = connection.cursor()
    cursor.execute(query)
    cursor.close()
    connection.close()


#creates the database if it does not exist
def db_init():
    create_db_query = "CREATE DATABASE IF NOT EXISTS library"
    run_query(create_connection(remove_database_config()),create_db_query)
    return "db created"


#creates the books table if it does not exist
def table_init():
    create_table_query = "CREATE TABLE IF NOT EXISTS books ("\
    "id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,"\
    " title VARCHAR(255) NOT NULL, author VARCHAR(255) NOT NULL,"\
    " UNIQUE title_author (title,author))"
    run_query(create_connection(config),create_table_query)
    return "table created"


#insert a new book record
def insert_new_book(title,author):
    add_book = ("INSERT INTO books "
               "(title, author) "
               "VALUES (%s, %s)")
    data_book = (title,author)
    #print(data_book, flush=True)
    get_new_book = ("SELECT * FROM books "
                    "WHERE id = %s")
    conn = create_connection(config)
    cursor = conn.cursor()
    cursor.execute(add_book,data_book)
    id = cursor.lastrowid
    conn.commit()
    cursor.execute(get_new_book,(id,))
    new_book = cursor.fetchall()
    cursor.close()
    conn.close()
    return {'id': new_book[0][0], 'title': new_book[0][1], 'author': new_book[0][2]}


def get_all_books():
    get_all_books = ("SELECT * FROM books ORDER BY 1")
    conn = create_connection(config)
    cursor = conn.cursor()
    cursor.execute(get_all_books)
    all_books = cursor.fetchall()
    cursor.close()
    conn.close()
    return [{'id': value[0], 'title': value[1], 'author': value[2]} for value in all_books]